import { css, } from 'lit-element';


export default css`:host {
  display: block;
  box-sizing: border-box;
  @apply --cells-ui-autocomplete-vcard; }

:host([hidden]), [hidden] {
  display: none !important; }

* {
  box-sizing: border-box; }

.white-bg bbva-form-field {
  --_field-bg-color:#fff !important; }

.autocomplete {
  position: relative;
  display: inline-block;
  width: 100%; }

.autocomplete-items {
  position: absolute;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  top: 100%;
  left: 0;
  right: 0;
  max-height: 240px;
  overflow-x: hidden;
  overflow-y: auto; }

.autocomplete-items div {
  padding: 15px 10px;
  cursor: pointer;
  background-color: #F4F4F4;
  border-bottom: 1px solid #BDBDBD;
  font-size: 0.9rem; }

.autocomplete-items div:hover {
  color: white;
  background-color: #1973B8; }

.autocomplete-active {
  color: white !important;
  background-color: #1973B8 !important; }

@keyframes lds-rolling {
  0% {
    -webkit-transform: translate(-50%, -50%) rotate(0deg);
    transform: translate(-50%, -50%) rotate(0deg); }
  100% {
    -webkit-transform: translate(-50%, -50%) rotate(360deg);
    transform: translate(-50%, -50%) rotate(360deg); } }

@-webkit-keyframes lds-rolling {
  0% {
    -webkit-transform: translate(-50%, -50%) rotate(0deg);
    transform: translate(-50%, -50%) rotate(0deg); }
  100% {
    -webkit-transform: translate(-50%, -50%) rotate(360deg);
    transform: translate(-50%, -50%) rotate(360deg); } }

.lds-rolling {
  position: absolute;
  z-index: 1000;
  top: 5px;
  right: 2px; }

.lds-rolling div,
.lds-rolling div:after {
  position: absolute;
  width: 150px;
  height: 150px;
  border: 10px solid #9f9f9f;
  border-top-color: transparent;
  border-radius: 50%; }

.lds-rolling div {
  -webkit-animation: lds-rolling 1s linear infinite;
  animation: lds-rolling 1s linear infinite;
  top: 100px;
  left: 100px; }

.lds-rolling div:after {
  -webkit-transform: rotate(90deg);
  transform: rotate(90deg); }

.lds-rolling {
  width: 35px !important;
  height: 35px !important;
  -webkit-transform: translate(-20px, -20px) scale(0.2) translate(20px, 20px);
  transform: translate(-20px, -20px) scale(0.2) translate(20px, 20px); }
`;