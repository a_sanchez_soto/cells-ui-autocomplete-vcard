import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-ui-autocomplete-vcard-styles.js';
import '@vcard-components/cells-util-behavior-vcard';
import '@bbva-web-components/bbva-form-field';
import '@vcard-components/cells-theme-vcard';
/**
This component ...

Example:

```html
<cells-ui-autocomplete-vcard></cells-ui-autocomplete-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
const utilBehavior = CellsBehaviors.cellsUtilBehaviorVcard;
export class CellsUiAutocompleteVcard extends utilBehavior(LitElement) {
  static get is() {
    return 'cells-ui-autocomplete-vcard';
  }

  // Declare properties
 // Declare properties
 static get properties() {
  return {
    label: { type: String },
    inputId: { type: String },
    currentFocus: { type: Number },
    items: { type: Object },
    rootItems: { type: String },
    displayLabel: { type: String },
    displayValue: { type: String },
    displayLabelCustom: { type: String },
    itemSelected: { type: Object },
    loading: { type: Boolean },
    remote: { type: Boolean },
    delaySearch: { type: Number },
    controlDelay: { type: Number },
    path: { type: String },
    params: { type: Object },
    method: { type: String },
    headers: { type: Object },
    host: { type: String },
    querySearch: { type: String },
    value: { type: String },
    bgClass: { type: String },
    name: { type: String }
  };
}

// Initialize properties
constructor() {
  super();
  this.value = '';
  this.currentFocus = -1;
  this.loading = false;
  this.delaySearch = 1000;
  this.controlDelay = 0;
  this.label = '';
  this.bgClass = '';
  let idRamdom = Math.floor(1 + Math.random() * (999999999));
  this.id = `autocomplete_${idRamdom}`;
  this.inputId = `${this.id}_input`;
  
  this.addEventListener('bbva-form-field-clear', async ()=> {
    this.itemSelected = {};
    this.value = '';
    this.dispatch('clear-value-autocomplete',{name: this.name})
    await this.requestUpdate();
  });

  this.updateComplete.then(() => {
    this.autocomplete();
  });
  this.items = [];
}

static get shadyStyles() {
  return `
    ${styles.cssText}
    ${getComponentSharedStyles('cells-autocomplete-vcard-shared-styles').cssText}
    ${getComponentSharedStyles('cells-theme-vcard').cssText}
    `;
}

displayListItemsAutocomplete(divItems, val, arrItems, isArryObjects) {
  let display;
  let item;
  let inp = this.shadowRoot.getElementById(this.inputId);
  for (let i = 0; i < arrItems.length; i++) {
    if (isArryObjects) {
      display = arrItems[i][this.displayLabel];
      if (this.displayLabelCustom) {
        display = this.labelCustomFormated(this.displayLabelCustom, arrItems[i]);
      }
    } else {
      display = arrItems[i];
    }
    if (display.toUpperCase().includes(val.toUpperCase())) {
      item = document.createElement('DIV');
      item.classList.add('container-list');
      let valTemp = display.substr(display.toUpperCase().indexOf(val.toUpperCase()), val.length);
      let re = new RegExp(valTemp, 'g');
      item.innerHTML = display.replace(re, '<strong>' + valTemp + '</strong>');
      item.innerHTML += '<input type="hidden" data-record = \'' + JSON.stringify(arrItems[i]) + '\' value="' + display + '" >';
      item.addEventListener('click', (e) => {
        let elementPathResult = e.path.filter((elementPath) => {
          if (elementPath.classList && elementPath.classList.contains) {
            return elementPath.classList.contains('container-list');
          }
          return false;
        });
        let inputEl = elementPathResult[0].getElementsByTagName('input')[0];
        if (inputEl && inputEl.dataset && inputEl.dataset.record) {
          this.itemSelected = JSON.parse(inputEl.dataset.record);
          this.dispatch('selected-item', this.itemSelected);
        }
        if (inputEl) {
          inp.value = inputEl.value;
        }
        this.closeAllLists();
      });
      divItems.appendChild(item);
    }
  }
}

buildAutocomplete(inp) {
  let divItems;
  let val = inp.value;
  this.closeAllLists();
  if (typeof val === 'undefined' || val === null) {
    return false;
  }
  this.currentFocus = -1;
  divItems = document.createElement('DIV');
  divItems.setAttribute('id', this.inputId + '-list');
  divItems.setAttribute('class', 'autocomplete-items');
  inp.parentNode.appendChild(divItems);

  let arrItems = [];
  if (this.rootItems) {
    arrItems = this.items[this.rootItems];
  } else {
    arrItems = this.items;
  }
  let isArryObjects = false;
  if (arrItems && arrItems.length > 0 && (arrItems[0] instanceof Object)) {
    isArryObjects = true;
  }

  this.displayListItemsAutocomplete(divItems, val, arrItems, isArryObjects);
}

async responseItemsRemote(response) {
  this.items = await response;
  this.loading = false;
  await this.requestUpdate();
  let inp = this.shadowRoot.getElementById(this.inputId);
  this.buildAutocomplete(inp);
}

queryApply(value, path) {
  this.loading = true;
  this.dispatch(
    'cells-autocomplete-vcard-query-event',
    {
      querySearch: value,
      onSuccess: (response) => {
        this.responseItemsRemote(response);
      },
      onError: (error) => {
        this.loading = false;
        console.log('Error al invocar el servicio remoto:', error);
      },
      path: path,
      method: this.method || 'GET',
      params: this.params,
      headers: this.headers,
      idDp: 'cotizaDepTest'
    }
  );
}

clearValue() {
  this.shadowRoot.getElementById(this.inputId).value = '';
}

focus() {
  this.shadowRoot.getElementById(this.inputId).focus();
}

addAtributo(name, value) {
  this.shadowRoot.getElementById(this.inputId).setAttribute(name, value);
}

removerAtributo(name) {
  this.shadowRoot.getElementById(this.inputId).removeAttribute(name);
}

setValue(value) {
  this.value = value;
}

autocomplete() {
  let inp = this.shadowRoot.getElementById(this.inputId);
  inp.addEventListener('input', (e) => {
    this.closeAllLists();

    if (this.remote) {
      clearTimeout(this.controlDelay);
      this.controlDelay = setTimeout(() => {
        let path = `${this.path}${this.querySearch}`;
        this.queryApply(inp.value, path);
      }, this.delaySearch);
    } else {
      this.buildAutocomplete(inp);
    }

  });

  inp.addEventListener('keyup', (e) => {
    if (!inp.value || inp.value.length === 0) {
      this.itemSelected = null;
      this.querySearch = '';
    } else {
      this.querySearch = inp.value;
    }
    this.dispatch('value-changed', { value: inp.value });
  });

  inp.addEventListener('keydown', (e) => {

    let x = this.shadowRoot.getElementById(this.inputId + '-list');
    let panelScroll = this.shadowRoot.querySelector('.autocomplete-items');
    if (x) {
      x = x.getElementsByTagName('div');
    }
    if (e.keyCode === 40) {
      if(panelScroll && typeof panelScroll.scrollTop !== 'undefined' ){
        if(this.currentFocus > 3) {
          panelScroll.scrollTop = panelScroll.scrollTop+70;  
        }     
      }
      this.currentFocus++;
      this.addActive(x);
    } else if (e.keyCode === 38) {
      this.currentFocus--;
      if(panelScroll && typeof panelScroll.scrollTop !== 'undefined' ){
        if(this.currentFocus > 3) {
          panelScroll.scrollTop = panelScroll.scrollTop-70;  
        }
      }
      this.addActive(x);
    } else if (e.keyCode === 13) {
      e.preventDefault();
      if (this.currentFocus > -1) {
        if (x) {
          x[this.currentFocus].click();
        }
      }
    } else if (e.keyCode === 27) {
      e.preventDefault();
      this.closeAllLists();
      if (!this.itemSelected) {
        this.shadowRoot.getElementById(this.inputId).value = '';
        this.shadowRoot.getElementById(this.inputId).invalid = false;
        this.shadowRoot.getElementById(this.inputId).focus();
      }

    }
  });

  document.addEventListener('click', (e) => {
    this.closeAllLists(e.target);
  });
}

addActive(x) {
  if (!x) {
    return false;
  }
  let panelScroll = this.shadowRoot.querySelector('.autocomplete-items');
  this.removeActive(x);
  if (this.currentFocus >= x.length) {
    this.currentFocus = 0;
    if(panelScroll) {
      panelScroll.scrollTop = 0;
    }
  }
  if (this.currentFocus < 0) {
    this.currentFocus = (x.length - 1);
  }
  x[this.currentFocus].classList.add('autocomplete-active');
}

removeActive(x) {
  for (var i = 0; i < x.length; i++) {
    x[i].classList.remove('autocomplete-active');
  }
}

closeAllLists(elmnt) {
  let x = this.shadowRoot.querySelectorAll('.autocomplete-items');
  let inp = this.shadowRoot.getElementById(this.inputId);
  for (let i = 0; i < x.length; i++) {
    if (elmnt !== x[i] && elmnt !== inp) {
      x[i].parentNode.removeChild(x[i]);
    }
  }
}

onBlurInput() {
  let inp = this.shadowRoot.getElementById(this.inputId);
  if (this.displayLabelCustom) {
    if (this.itemSelected) {
      this.shadowRoot.getElementById(this.inputId).value = this.labelCustomFormated(this.displayLabelCustom, this.itemSelected);
    }
  } else {
    if (this.itemSelected) {
      this.shadowRoot.getElementById(this.inputId).value = this.itemSelected[this.displayLabel];
    }
  }

  if (!this.itemSelected) {
    this.shadowRoot.getElementById(this.inputId).value = '';
  }

  this.dispatch('on-blur', this.shadowRoot.querySelectorAll('.autocomplete-items').length);

}

// Define a template
render() {
  return html`
  <style>${this.constructor.shadyStyles}</style>
  <slot></slot>
  <div class="autocomplete ${this.bgClass}">
  <div ?hidden = ${!this.loading} class="lds-css ng-scope"><div class="lds-rolling"><div style = "background-color: #fff;" ></div></div></div>
    <bbva-form-field  value = "${this.value}" label="${this.label}" @blur = "${(e) => {
if (e.detail !== null) {
  this.onBlurInput();
}
}}"  id = "${this.inputId}"  ></bbva-form-field>
  </div>
  `;
}
}

// Register the element with the browser
customElements.define(CellsUiAutocompleteVcard.is, CellsUiAutocompleteVcard);
