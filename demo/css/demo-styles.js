import { setDocumentCustomStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import { css, } from 'lit-element';

setDocumentCustomStyles(css`
  #iframeBody {
    margin: 20px;

  }
  body {
    font-family: var(--cells-fontDefault, sans-serif);
    margin: 15px;
    background-color:#f4f4f4;
  }

  .card {
    /* Add shadows to create the "card" effect */
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    background-color:#fff;
  }
  
  /* On mouse-over, add a deeper shadow */
  .card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
  }
  
  /* Add some padding inside the card container */
  .container {
    padding: 15px;
  }

  h3 {
    color: #1464A5;
    font-size:1.2em;
    margin:0px 0px 5px 5px;
  }

  .second-panel {
    margin-top:20px;
  }

  .hidden {
    display:none;
  }

  .root {
    max-height:400px;
    overflow:auto;
  }

`);
